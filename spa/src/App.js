import React from 'react';
import Contacts from './components/contacts';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div className="App">
      <Contacts />
    </div>
  );
}

export default App;
