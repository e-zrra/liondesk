const API_URL = 'http://localhost:3333/api';

export async function getAllContact ({ search }) {
  try {
    const response = await fetch(`${API_URL}/contacts?search=${search}`);
    const result = await response.json();

    return result;

  } catch (error) {
    return { success: false };
  }
}

export async function saveContact({ contact }) {
  try {
    const options = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(contact),
    };
    const response = await fetch(`${API_URL}/contacts`, options);
    const result = await response.json();

    return result;
  } catch (error) {
    return { success: false };
  }
}