import React from 'react';
import { Alert, Button, Col, Form, Modal, Spinner, Table } from 'react-bootstrap';
import { getAllContact, saveContact } from '../../actions/contacts';

export default class Contacts extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      contacts: [],
      contactAdded: false,
      saveError: false,
      saveErrorMessage: '',
      contactsError: false,
      contactsErrorMessage: '',
      contactsLoading: false,
      saveLoading: false,
      contactsTotal: 0,
      showAddContactModal: false,
      contact: {
        first_name: '',
        last_name: '',
        email: '',
        city: '',
        phone: '',
        age: '',
        company: '',
      },
      isValidForm: false,
      search: '',
    }

    this.handleShowAddContactModal = this.handleShowAddContactModal.bind(this);
    this.handleCloseAddContactModal = this.handleCloseAddContactModal.bind(this);
    this.handleSaveContact = this.handleSaveContact.bind(this);
    this.handleSearchClick = this.handleSearchClick.bind(this);
  }

  handleShowAddContactModal() {
    this.setState({ showAddContactModal: true });
  }

  handleCloseAddContactModal() {
    this.setState({ showAddContactModal: false });
  }

  handleFirstNameChange = (event) => {
    const value = event.target.value;
    this.setState({ contact: { ...this.state.contact, first_name: value }}, () => {
      this.validateForm();
    });
  }

  handleLastNameChange = (event) => {
    const value = event.target.value;

    this.setState({ contact: { ...this.state.contact, last_name: value }}, () => {
      this.validateForm();
    });
  }

  handleEmailChange = (event) => {
    const value = event.target.value;

    this.setState({ contact: { ...this.state.contact, email: value }}, () => {
      this.validateForm();
    });
  }

  handleCityChange = (event) => {
    const value = event.target.value;

    this.setState({ contact: { ...this.state.contact, city: value }}, () => {
      this.validateForm();
    });
  }

  handlePhoneChange = (event) => {
    const value = event.target.value;

    this.setState({ contact: { ...this.state.contact, phone: value }}, () => {
      this.validateForm();
    });
  }

  handleAgeChange = (event) => {
    const value = event.target.value;

    this.setState({ contact: { ...this.state.contact, age: value }}, () => {
      this.validateForm();
    });
  }

  handleCompanyChange = (event) => {
    const value = event.target.value;

    this.setState({ contact: { ...this.state.contact, company: value }}, () => {
      this.validateForm();
    });
  }

  async loadContacts ({ search }) {
    this.setState({ contactsLoading: true });

    const result = await getAllContact({ search });
    const { count, data, success } = result;

    if (success) {
      this.setState({
        contacts: data,
        contactsTotal: count,
        contactsLoading: false,
      });
    } else {
      this.setState({
        contactsError: true,
        contactsErrorMessage: 'Error from server',
        contactsLoading: false,
      });
    }
  }

  async componentDidMount() {
    await this.loadContacts({ search: '' });
  }

  validateForm = () => {
    const { contact } = this.state;

    if (contact.first_name !== "" && contact.last_name !== "" && contact.email !== ""
      && contact.city !== "" && contact.phone !== ""
      && contact.age !== "" && contact.company !== "") {
      this.setState({ isValidForm: true });
    } else {
      this.setState({ isValidForm: false });
    }
  }

  async handleSaveContact() {
    const { contact } = this.state;

    this.setState({ saveLoading: true, saveError: false, saveErrorMessage: '', contactAdded: false });

    const result = await saveContact({ contact });
    const { success } = result;

    if (success) {
      this.handleCloseAddContactModal();
      this.setState({
        saveLoading: false,
        contactAdded: true,
        contact: {
          first_name: '',
          last_name: '',
          email: '',
          city: '',
          phone: '',
          age: '',
          company: '',
        },
      });
    } else {
      this.setState({
        saveError: true,
        saveErrorMessage: 'Error from server',
        saveLoading: false,
      });
    }
  }

  handleSearch = (event) => {
    const value = event.target.value;

    this.setState({ search: value });
  }

  renderContactsTotal = () => {
    const { contactsTotal, contactsLoading } = this.state;

    if (!contactsLoading) {
      return <div><b>Results:</b> {contactsTotal}</div>;
    }

    return null;
  }

  renderMessageError = () => {
    const { contactsError, contactsErrorMessage } = this.state;

    if (contactsError) {
      return <div><b>Error:</b> {contactsErrorMessage}</div>;
    }

    return null;
  }

  renderSabeButtonOrLoading = () => {
    const { saveLoading, isValidForm } = this.state;

    if (saveLoading) {
      return (
        <Spinner animation="border" role="status">
          <span className="sr-only">Loading...</span>
        </Spinner>
      )
    }

    return <Button variant="success" disabled={!isValidForm} onClick={this.handleSaveContact}>Save</Button>;
  }

  renderSaveError = () => {
    const { saveError, saveErrorMessage } = this.state;

    if (saveError) {
      return <div>Error: {saveErrorMessage}</div>
    }

    return null;
  }

  renderAddContactModal = () => {
    const { first_name, last_name, email, city, phone, age, company } = this.state.contact;

    return (
      <Modal show={this.state.showAddContactModal} onHide={this.handleCloseAddContactModal}>
        <Modal.Header closeButton>
          <Modal.Title>Add contact</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group>
              <Form.Label>First name</Form.Label>
              <Form.Control required onChange={this.handleFirstNameChange} value={first_name} type="text" placeholder="Myers" />
            </Form.Group>

            <Form.Group>
              <Form.Label>Last name</Form.Label>
              <Form.Control onChange={this.handleLastNameChange} value={last_name}  type="text" placeholder="Stephenson" />
            </Form.Group>

            <Form.Group>
              <Form.Label>Email</Form.Label>
              <Form.Control onChange={this.handleEmailChange} value={email}  type="email" placeholder="myers.stephenson@miracula.com" />
            </Form.Group>

            <Form.Group>
              <Form.Label>City</Form.Label>
              <Form.Control onChange={this.handleCityChange} value={city}  type="text" placeholder="Rivers" />
            </Form.Group>

            <Form.Group>
              <Form.Label>Phone</Form.Label>
              <Form.Control onChange={this.handlePhoneChange} value={phone}  type="text" placeholder="+1 (829) 440-3085" />
            </Form.Group>

            <Form.Group>
              <Form.Label>Age</Form.Label>
              <Form.Control onChange={this.handleAgeChange} value={age}  type="number" placeholder="37" />
            </Form.Group>

            <Form.Group>
              <Form.Label>Company</Form.Label>
              <Form.Control onChange={this.handleCompanyChange} value={company}  type="text" placeholder="Miracula" />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          {this.renderSaveError()}
          {this.renderSabeButtonOrLoading()}
          <Button variant="default" onClick={this.handleCloseAddContactModal}>Close</Button>
        </Modal.Footer>
      </Modal>
    )
  }

  renderContactAdded = () => {
    if (this.state.contactAdded) {
      return (
        <Alert variant={"success"}>
          Contact added
        </Alert>
      )
    }

    return null;
  }

  handleSearchClick = async () => {
    const { search } = this.state;
    await this.loadContacts({ search });
  }

  render() {
    return (
      <div>
        <h2>Contacts</h2>
        <Button bsStyle="success" onClick={this.handleShowAddContactModal}>+ Add contact</Button>
        <hr />
        {this.renderContactAdded()}
        {this.renderAddContactModal()}
        {this.renderContactsTotal()}
        {this.renderMessageError()}

        <Form.Row>
          <Col>
            <Form.Control value={this.state.search} onChange={this.handleSearch} type="text" placeholder="myers.stephenson@miracula.com" />
          </Col>
          <Col>
            <Button variant="primary" type="submit" onClick={this.handleSearchClick}>
              Search
            </Button>
          </Col>
        </Form.Row>
        
        <br/>

        <Table striped bordered condensed hover>
          <thead>
            <tr>
              <th>Name</th>
              <th>Last Name</th>
              <th>Email</th>
              <th>City</th>
              <th>Phone</th>
              <th>Age</th>
              <th>Company</th>
            </tr>
          </thead>
          <tbody>
            {this.state.contacts.map((contact) => {
              return (
                <tr key={contact._id}>
                  <td>
                    {contact.first_name}
                  </td>
                  <td>
                    {contact.last_name}
                  </td>
                  <td>
                    {contact.email}
                  </td>
                  <td>
                    {contact.city}
                  </td>
                  <td>
                    {contact.phone}
                  </td>
                  <td>
                    {contact.age}
                  </td>
                  <td>
                    {contact.company}
                  </td>
                </tr>
              )
            })}
          </tbody>
        </Table>
      </div>
    );
  }
}