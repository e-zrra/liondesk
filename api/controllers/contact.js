const Contact = require('../models/contact');

getAllContact = async (req, res) => {
  try {
    let data;
    const { search } = req.query;

    if (search) {
      data = await Contact.find({
        "$or": [
          { first_name: { $regex: search , $options: 'i'} },
          { last_name: { $regex: search , $options: 'i'} },
          { email: { $regex: search , $options: 'i'} },
          { city: { $regex: search , $options: 'i'} },
          { age: { $regex: search , $options: 'i'} },
          { company: { $regex: search , $options: 'i'} },
          { phone: { $regex: search , $options: 'i'} },
          { city: { $regex: search , $options: 'i'} }
        ]
      });
    } else {
      data = await Contact.find({});
    }
    
    const count = data.length;

    res.status(200).json({ count, data, success: true });
  } catch (error) {
    res.status(500).json({ error, success: false });
  }
}

createContact = async (req, res) => {
  try {
    const { body } = req;

    if (!body) {
      res.status(400).json({ success: false });
    }

    const contact = new Contact(body);

    if (!contact) {
      res.status(400).json({ success: false });
    }

    const result = await contact.save();

    if (result._id) {
      res.status(200).json({ success: true, data: result });
    } else {
      res.status(500).json({ success: false });
    }
  } catch (error) {
    res.status(500).json({ error, success: false });
  }
}

module.exports = {
  createContact,
  getAllContact,
}