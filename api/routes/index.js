const routes = require('express').Router();
const contactRoutes = require('./contact');

routes.use('/contacts', contactRoutes);

module.exports = routes;