const router = require('express').Router();
const contactController = require('../controllers/contact');

router.get('/', contactController.getAllContact);
router.post('/', contactController.createContact);

module.exports = router;
