# LionDesk

## Configuration mongodb

### Install and run mongodb

<https://treehouse.github.io/installation-guides/mac/mongo-mac.html>

### Install

```
$ brew install mongodb
```

### Create data folde

```
$ mkdir -p /data/db
```

### Run mongodb server

```
$ mongod
```

## Create database

```
$ mongo
```

```
$ > use liondesk
```

## Insert data

```
$ > db.contacts.insertMany( [
  {
    "name": "Mauricio",
    "last_name": "Magre",
    "email": "mmaigre@gmail.com",
    "city": "Tijuana"
  },
  {
    "name": "Esli",
    "last_name": "Salgado",
    "email": "esalgado@gmail.com",
    "city": "Tijuana"
  }
]);
```

## Install dependecies

```
$ yarn
```

## Run server

```
$ yarn start
```

## Run dev

```
$ yarn dev
```