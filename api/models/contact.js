const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Contact = new Schema({
  first_name: { type: String, require: true },
  last_name: { type: String, require: true },
  email: { type: String, require: true },
  city: { type: String, require: false },
  age: { type: String, require: false },
  company: { type: String, require: false },
  phone: { type: String, require: false },
  city: { type: String, require: false },
});

module.exports = mongoose.model('contact', Contact);
