const mongoose = require('mongoose');

async function connect() {
  try {
    await mongoose.connect('mongodb://127.0.0.1:27017/liondesk', { useNewUrlParser: true });
  } catch (error) {
    console.error(`Connection error: ${error.message}`);
  }
}

module.exports = connect();