const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();
const port = 3333;
const database = require('./database');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.use(bodyParser.json());

database.catch(error => {
  console.error(`Error connection: ${error}`);
});

const routes = require('./routes');

app.get('/', (_, res) => {
  res.send('Lion Desk');
});

app.use('/api', routes);

app.listen(port, () => {
  console.info(`Server running on port ${port}`);
});

